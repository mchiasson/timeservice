var searchData=
[
  ['semaphore',['Semaphore',['../classSemaphore.html',1,'Semaphore&lt; Mutex &gt;'],['../classSemaphore.html#a89859a748509a8df31db98c8d72d05b3',1,'Semaphore::Semaphore(uint32_t nStartingResourceCount=0)'],['../classSemaphore.html#a0d64c20de1473d07dc4dfde64f414bb2',1,'Semaphore::Semaphore(const Semaphore &amp;other)=delete'],['../classSemaphore.html#ae7221f2d7b71e1fdd6a36b70e4e02f9d',1,'Semaphore::Semaphore(const Semaphore &amp;&amp;other)=delete']]],
  ['semaphore_3c_20spinlock_20_3e',['Semaphore&lt; SpinLock &gt;',['../classSemaphore.html',1,'']]],
  ['setinterval',['setInterval',['../classTimer.html#a9b741c9e9746c369c34ad4c99c47ea82',1,'Timer']]],
  ['setpopmode',['setPopMode',['../classConcurrentQueue.html#aa2de1e53b54ce20c654c99aa1b13d63a',1,'ConcurrentQueue']]],
  ['setsingleshot',['setSingleShot',['../classTimer.html#a8a5c81c4b4d1aea1cd159b9c4474ee86',1,'Timer']]],
  ['settimeoutcallback',['setTimeoutCallback',['../classTimer.html#ac9668bcf0bbe1e2e22b6c80fbb1d8b7e',1,'Timer']]],
  ['settimeservice',['setTimeService',['../classTimer.html#abf5fd03510f3d19a09504d15362cebf3',1,'Timer']]],
  ['sort',['sort',['../classConcurrentSortedList.html#a0835f8f90bbc8660fd0bc3bfc24a4c12',1,'ConcurrentSortedList']]],
  ['spinlock',['SpinLock',['../classSpinLock.html',1,'SpinLock'],['../classSpinLock.html#a7dc17bb50466088acb8ea4a45d43b183',1,'SpinLock::SpinLock()'],['../classSpinLock.html#a9214d3d4ee8fb6a27aad44d2cb929d74',1,'SpinLock::SpinLock(const SpinLock &amp;other)=delete'],['../classSpinLock.html#abda55fa1146a15767186dcf5e05dfad9',1,'SpinLock::SpinLock(const SpinLock &amp;&amp;other)=delete']]],
  ['start',['start',['../classTimer.html#a23dc10d310fcd5bb860fec3cb0d0bdc5',1,'Timer::start(const std::chrono::milliseconds &amp;intervalMs)'],['../classTimer.html#a3a8b5272198d029779dc9302a54305a8',1,'Timer::start()']]],
  ['stop',['stop',['../classTimer.html#a63f0eb44b27402196590a03781515dba',1,'Timer']]]
];
