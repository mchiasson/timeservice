##############################################################################
# The MIT License (MIT)
#
# Copyright © 2015 Mathieu-Andre Chiasson
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
##############################################################################

cmake_minimum_required(VERSION 2.8.9)

project(timeservice)

if(CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -Wextra -pedantic")
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g3")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Ofast")
endif()

set(SOURCES
    src/EventQueue.cpp
    src/EventQueuePriv.cpp
    src/SpinLock.cpp
    src/TimeoutEvent.cpp
    src/Timer.cpp
    src/TimerPriv.cpp
    src/TimeService.cpp
    src/TimeServicePriv.cpp
)

set(PRIVATE_HEADERS
    src/EventQueuePriv.h
    src/TimerData.h
    src/TimerPriv.h
    src/TimeServicePriv.h
)

set(PUBLIC_HEADERS
    include/timeservice/ConcurrentQueue.h
    include/timeservice/ConcurrentSortedList.h
    include/timeservice/EventQueue.h
    include/timeservice/IEvent.h
    include/timeservice/ITimeoutEventHandler.h
    include/timeservice/ReadWriteMutex.h
    include/timeservice/Semaphore.h
    include/timeservice/SpinLock.h
    include/timeservice/TimeoutEvent.h
    include/timeservice/Timer.h
    include/timeservice/TimeService.h
)

include_directories(
    ${PROJECT_SOURCE_DIR}/include
)

add_library(${PROJECT_NAME} STATIC ${SOURCES} ${PRIVATE_HEADERS} ${PUBLIC_HEADERS})
