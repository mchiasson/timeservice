/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

#ifndef READWRITEMUTEX_H
#define READWRITEMUTEX_H

#include <timeservice/Semaphore.h>

/*!
 * Simple Read/Write Mutex.
 *
 * This type of mutex mechanism is available in C++14, known as std::shared_mutex, where read-lockers would call
 * lock_shared()/try_lock_shared(), and writers would call the regular lock()/try_lock(). Unfortunately, since the
 * code was written for C++11, I had to implement my own.
 *
 * readLocks are always going to run in parallel, while writeLocks are exclusive locks like regular mutex. If a
 * writeLock has been requested while one or many readlocks are currently running, it will block until all the readLocks
 * are calling readUnlock().
 *
 * This implementation also ensure that read-lockers are not going to starve the write-lockers, but comes at a cost. For
 * this reason, the maximum amount of readers is set to 32 by default, but bare in mind that the more readers you want,
 * the slower it will be to lock this mutex for writing.
 *
 * ReadWriteMutex is non-copyable and non-movable
 */
template<class Mutex = std::mutex>
class ReadWriteMutex
{
public:
    /*!
     * \brief ReadWriteMutex Default constructor.
     *
     * Initializes the internal semaphore with a maximum amount of resrouces.
     */
    ReadWriteMutex(uint32_t nMaximumReaderCount = 32);

    /*!
     * Copy constructor is disabled.
     */
    ReadWriteMutex(const ReadWriteMutex &other) = delete;

    /*!
     * Move constructor is disabled.
     */
    ReadWriteMutex(const ReadWriteMutex &&other) = delete;

    /*!
     * Assignment operator is disabled.
     */
    ReadWriteMutex &operator=(const ReadWriteMutex &other) = delete;

    /*!
     * Move assignment operator is disabled.
     */
    ReadWriteMutex &operator=(const ReadWriteMutex &&other) = delete;

    /*!
     * \brief readLock attempts to perform a read-lock
     *
     * If there is a writer currently writing, this function or if the maximum allowed readers have been reached,
     * tryReadLock() will fail and the function will return false. Othwerise, it will return true;
     */
    bool tryReadLock();

    /*!
     * \brief readLock performs a read-lock
     *
     * If there is a writer currently writing, or if the maximum allowed readers have been reached, readLock() will
     * block until writeUnlock() is called, or until the last allowed reader calls readUnblock()
     */
    void readLock();

    /*!
     * \brief readUnlock notify that the read operation was performed and completed.
     *
     * This function will wake up every write-lockers waiting on the writeLock() function, but only if there are no
     * other readers currently holding a readlock.
     */
    void readUnlock();

    /*!
     * \brief writeLock attempts to perform a write-lock
     *
     * If there are one or more read-lock or another write-lock being performed, tryWriteLock will fail and this
     * function will return false. Otherwise, it will return true.
     */
    bool tryWriteLock();

    /*!
     * \brief writeLock performs a write-lock
     *
     * If there are one or more read-lock or another write-lock being performed, writeLock will block until every
     * read-lockers currently reading are all calling readUnlock(). New readers calling readLock() and new writers also
     * calling writeLock() won't be allowed to jump the queue.
     */
    void writeLock();

    /*!
     * \brief writeUnlock notify that the write operation was performed and completed.
     *
     * This function will wake up every readers and writers taht are blocked in readLock() and writeLock().
     */
    void writeUnlock();

private:

    Semaphore<Mutex> m_semaphore;
    Mutex            m_writeLockMutex;
};

/*!
 * RAII Container which performs ReadWriteMutex::readLock() and ReadWriteMutex::readUnlock() for you
 */
template<class Mutex = std::mutex>
class ReadLockGuard
{
public:

    /*!
     * \brief ReadLockGuard Constructor
     * \param mutex the mutex to track
     *
     * Performs ReadWriteMutex::readLock()
     */
    ReadLockGuard(ReadWriteMutex<Mutex> &mutex);

    /*!
     * \brief ReadLockGuard Destructor
     *
     * Performs ReadWriteMutex::readUnlock()
     */
    ~ReadLockGuard();

private:

    ReadWriteMutex<Mutex> &m_mutex;
};

/*!
 * RAII Container which performs ReadWriteMutex::writeLock() and ReadWriteMutex::writeUnlock() for you
 */
template<class Mutex = std::mutex>
class WriteLockGuard
{
public:

    /*!
     * \brief WriteLockGuard Constructor
     * \param mutex the mutex to track
     *
     * Performs ReadWriteMutex::writeLock()
     */
    WriteLockGuard(ReadWriteMutex<Mutex> &mutex);

    /*!
     * \brief WriteLockGuard Destructor
     *
     * Performs ReadWriteMutex::writeUnlock()
     */
    ~WriteLockGuard();

private:

    ReadWriteMutex<Mutex> &m_mutex;
};

typedef ReadWriteMutex<SpinLock> FastReadWriteMutex;
typedef ReadLockGuard<SpinLock>  FastReadLockGuard;
typedef WriteLockGuard<SpinLock> FastWriteLockGuard;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template<class Mutex>
inline ReadWriteMutex<Mutex>::ReadWriteMutex(uint32_t nMaximumReaderCount) :
    m_semaphore(nMaximumReaderCount)
{
}


template<class Mutex>
inline bool ReadWriteMutex<Mutex>::tryReadLock()
{
    return m_semaphore.tryAcquire(1);
}

template<class Mutex>
inline void ReadWriteMutex<Mutex>::readLock()
{
    m_semaphore.acquire(1);
}

template<class Mutex>
inline void ReadWriteMutex<Mutex>::readUnlock()
{
    m_semaphore.release(1);
}

template<class Mutex>
inline bool ReadWriteMutex<Mutex>::tryWriteLock()
{
    // prevent more than one writer to come-in at a time
    std::lock_guard<Mutex> lock(m_writeLockMutex);

    // grab as many resources as we can until we obtain every resources from the semaphore. This is how we prevent
    // other readers from jumping the queue. It's only fair, but it comes at a big cost since we cannot acquire all of
    // our resources from the mutex in one shot (in one mutex lock).
    for (uint32_t i = 0, max = m_semaphore.getStartingResourceCount(); i < max; ++i)
    {
        if (!m_semaphore.tryAcquire(1))
        {
            m_semaphore.release(i);
            return false;
        }
    }

    return true;
}

template<class Mutex>
inline void ReadWriteMutex<Mutex>::writeLock()
{
    // prevent more than one writer to come-in at a time
    std::lock_guard<Mutex> lock(m_writeLockMutex);

    // grab as many resources as we can until we obtain every resources from the semaphore. This is how we prevent
    // other readers from jumping the queue. It's only fair, but it comes at a big cost since we cannot acquire all of
    // our resources from the mutex in one shot (in one mutex lock).
    for (uint32_t i = 0, max = m_semaphore.getStartingResourceCount(); i < max; ++i)
    {
        m_semaphore.acquire(1);
    }
}

template<class Mutex>
inline void ReadWriteMutex<Mutex>::writeUnlock()
{
    m_semaphore.release(m_semaphore.getStartingResourceCount());
}

template<class Mutex>
inline ReadLockGuard<Mutex>::ReadLockGuard(ReadWriteMutex<Mutex> &mutex) :
    m_mutex(mutex)
{
    m_mutex.readLock();
}

template<class Mutex>
inline ReadLockGuard<Mutex>::~ReadLockGuard()
{
    m_mutex.readUnlock();
}

template<class Mutex>
inline WriteLockGuard<Mutex>::WriteLockGuard(ReadWriteMutex<Mutex> &mutex) :
    m_mutex(mutex)
{
    m_mutex.writeLock();
}

template<class Mutex>
inline WriteLockGuard<Mutex>::~WriteLockGuard()
{
    m_mutex.writeUnlock();
}

#endif // READWRITEMUTEX_H
