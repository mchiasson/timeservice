/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

#ifndef CONCURRENTSORTEDLIST_H
#define CONCURRENTSORTEDLIST_H

#include <algorithm>
#include <functional>
#include <vector>

#include <timeservice/ReadWriteMutex.h>

/*!
 * ConcurrentSortedList is a thread-safe list that sorts automatically based on your desired predicate rule. It also
 * uses a read/write lock which allows multi-threaded reads to happen in parallel while write access would be blocking.
 * This way, the list can be traversed while a sorted-insertion occurs.
 */
template<class T, class Comparator = std::less<T> >
class ConcurrentSortedList
{
public:

    typedef std::function< bool(T &) >       IterateCallback;
    typedef std::function< bool(const T &) > IterateConstCallback;

    /*!
     * \brief ConcurrentSortedList Constructor
     * \param reserve optional reserve size for initial performance improvement.
     */
    ConcurrentSortedList(size_t reserve = 64);

    /*!
     * \brief ConcurrentSortedList Copy Constructor
     * \param other the other concurrent-sorted list container to copy from.
     */
    ConcurrentSortedList(const ConcurrentSortedList &other);

    /*!
     * \brief ConcurrentSortedList Move Constructor
     * \param other the other concurrent-sorted list container to move from.
     */
    ConcurrentSortedList(const ConcurrentSortedList &&other);
    ~ConcurrentSortedList();

    /*!
     * \brief operator= Assignment operator
     * \param other the other concurrent-sorted list container to copy from.
     * \return
     */
    ConcurrentSortedList &operator=(const ConcurrentSortedList &other);

    /*!
     * \brief operator= Move assignment operator
     * \param other the other concurrent-sorted list container to move from.
     * \return
     */
    ConcurrentSortedList &operator=(const ConcurrentSortedList &&other);

    /*!
     * \brief insert inserts a new element in the list.
     *
     * The insertion will be automatically sorted without iterating through the entire list for performance reason.
     * Duplicated values are allowed.
     *
     * \param value the value to insert
     *
     * \note this function performs a write-lock.
     */
    void insert(const T &value);

    /*!
     * \brief insert inserts a new element in the list if the value is not present
     *
     * The insertion will be automatically sorted without iterating through the entire list for performance reason.
     * Duplicated values are disallowed.
     *
     * \param value the value to insert
     *
     * \note this function performs a write-lock.
     */
    void insertUnique(const T &value);

    /*!
     * \brief erase erases the first occurence from list with a given value.
     *
     * \param value the value to erase
     *
     * \note this function performs a write-lock.
     */
    void erase(const T &value);

    /*!
     * \brief erase erases every occurences from list with a given value.
     *
     * \param value the value to erase
     *
     * \note this function performs a write-lock.
     */
    void eraseAll(const T &value);

    /*!
     * \brief iterate Traverses the entire list with a given callback.
     *
     * The callback will be called for every element of the list. The list will be read-lock protected during this
     * operation. The callback requires the user to return a boolean. If the callback returns true, it indicates that
     * the user still wants to iterate. If the call back returns false, it indicates the the user doesn't want
     * to iterate anymore, that it wants the iteration to stop.
     *
     * This overloaded variant allows read/write access to T value.
     *
     * \param callback the user callback to receive every element of the list.
     *
     * \note this function performs a read-lock
     */
    void iterate(const IterateCallback &callback);

    /*!
     * \brief iterate Traverses the entire list with a given callback.
     *
     * The callback will be called for every element of the list. The list will be read-lock protected during this
     * operation. The callback requires the user to return a boolean. If the callback returns true, it indicates that
     * the user still wants to iterate. If the call back returns false, it indicates the the user doesn't want
     * to iterate anymore, that it wants the iteration to stop.
     *
     * This overloaded variant allows read-only access to T value
     *
     * \param callback the user callback to receive every element of the list.
     *
     * \note this function performs a read-lock
     */
    void iterate(const IterateConstCallback &callback) const;

    /*!
     * \brief sort re-sort the list.
     *
     * This function will resort the list. This is useful when the T value has changed during iterate(), and the list
     * needs to be resorted accordingly.
     */
    void sort();

    /*!
     * \brief clear Clears the list.
     *
     * \note this function performs a write-lock.
     */
    void clear();

private:

    mutable FastReadWriteMutex m_mutex;
    std::vector<T>             m_container;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T, class Comparator>
inline ConcurrentSortedList<T, Comparator>::ConcurrentSortedList(size_t reserve)
{
    m_container.reserve(reserve);
}

template<class T, class Comparator>
inline ConcurrentSortedList<T, Comparator>::ConcurrentSortedList(const ConcurrentSortedList &other)
{
    FastReadLockGuard readLock(other.m_mutex);
    m_container = other.m_container;
}

template<class T, class Comparator>
inline ConcurrentSortedList<T, Comparator>::ConcurrentSortedList(const ConcurrentSortedList &&other) :
    m_container(std::move(other.m_container))
{
}

template<class T, class Comparator>
inline ConcurrentSortedList<T, Comparator>::~ConcurrentSortedList()
{
    clear();
}

template<class T, class Comparator>
inline ConcurrentSortedList<T, Comparator> &ConcurrentSortedList<T, Comparator>::operator=(const ConcurrentSortedList &other)
{
    if (this != &other)
    {
        FastReadLockGuard otherReadLock(other.m_mutex);
        FastWriteLockGuard writeLock(m_mutex);
        m_container = other.m_container;
    }

    return *this;
}

template<class T, class Comparator>
inline ConcurrentSortedList<T, Comparator> &ConcurrentSortedList<T, Comparator>::operator=(const ConcurrentSortedList &&other)
{
    if (this != &other)
    {
        FastWriteLockGuard writeLock(m_mutex);
        m_container = std::move(other.m_container);
    }

    return *this;
}


template<class T, class Comparator>
inline void ConcurrentSortedList<T, Comparator>::insert(const T &value)
{
    FastWriteLockGuard writeLock(m_mutex);

    Comparator comparator;

    auto it = m_container.begin();
    auto end = m_container.end();

    while (it != end)
    {
        if (comparator(value, *it))
        {
            m_container.emplace(it, value);
            return;
        }
        ++it;
    }

    m_container.push_back(value);
}

template<class T, class Comparator>
inline void ConcurrentSortedList<T, Comparator>::insertUnique(const T &value)
{
    FastWriteLockGuard writeLock(m_mutex);

    Comparator comparator;

    auto it = m_container.begin();
    auto end = m_container.end();

    // since the vector is sorted, we can use a binary search to find the existing value faster
    if (std::binary_search(it, end, value) == false)
    {
        while (it != end)
        {
            if (comparator(value, *it))
            {
                m_container.emplace(it, value);
                return;
            }
            ++it;
        }

        m_container.push_back(value);
    }
}

template<class T, class Comparator>
inline void ConcurrentSortedList<T, Comparator>::erase(const T &value)
{
    FastWriteLockGuard writeLock(m_mutex);

    auto it = m_container.begin();
    auto end = m_container.end();
    while (it != end)
    {
        if (value == *it)
        {
            m_container.erase(it);
            return;
        }
        ++it;
    }
}

template<class T, class Comparator>
inline void ConcurrentSortedList<T, Comparator>::eraseAll(const T &value)
{
    FastWriteLockGuard writeLock(m_mutex);

    auto begin = m_container.begin();
    auto end = m_container.end();

    m_container.erase(std::remove(begin, end, value), end);
}


template<class T, class Comparator>
inline void ConcurrentSortedList<T, Comparator>::iterate(const IterateCallback &callback)
{
    FastReadLockGuard readLock(m_mutex);

    for (auto &value : m_container)
    {
        // stop if our callback returned false
        if (!callback(value))
        {
            break;
        }
    }
}

template<class T, class Comparator>
inline void ConcurrentSortedList<T, Comparator>::iterate(const IterateConstCallback &callback) const
{
    FastReadLockGuard readLock(m_mutex);

    for (const auto &value : m_container)
    {
        // stop if our callback returned false
        if (!callback(value))
        {
            break;
        }
    }
}

template<class T, class Comparator>
inline void ConcurrentSortedList<T, Comparator>::sort()
{
    FastWriteLockGuard writeLock(m_mutex);
    std::sort(m_container.begin(), m_container.end(), Comparator());
}

template<class T, class Comparator>
inline void ConcurrentSortedList<T, Comparator>::clear()
{
    FastWriteLockGuard writeLock(m_mutex);
    m_container.clear();
}

#endif // CONCURRENTSORTEDLIST_H

