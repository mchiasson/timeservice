/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

#ifndef CONCURRENTQUEUE
#define CONCURRENTQUEUE

#include <condition_variable>
#include <mutex>
#include <queue>

/*!
 * \brief The PopMode enum for the ConcurrentQueue
 */
enum PopMode
{
    /*!
     * When the user calls pop and the queue is empty, it will returned imediately.
     */
    PopModeNonBlocking = 0,

    /*!
     * When the user calls pop and the queue is empty, it will block until data becomes available. The user can
     * also optionall unblock the queue by changing the PopMode by calling setPopMode(PopModeNonBlocking).
     */
    PopModeBlocking,
};

/*!
 * A thread-safe queue that blocks the consumer until data has been pushed. The user can manually unblock the consumer
 * by calling unblock().
 *
 * The consumer is automatically unblocked if the ConcurrentQueue is destroyed
 */
template<class T, class Queue = std::queue<T>, class Mutex = std::mutex >
class ConcurrentQueue
{
public:

    /*!
     * \brief ConcurrentQueue constructor
     * \param popMode Set to PopModeBlocking if you want pop to block the calling thread until data arrives, set to
     * PopModeNonBlocking otherwise.
     */
    ConcurrentQueue(PopMode ePopMode);

    /*!
     * \brief ConcurrentQueue Default Copy Constructor
     * \param other the other concurrent queue to copy
     */
    ConcurrentQueue(const ConcurrentQueue &other);

    /*!
     * \brief ConcurrentQueue Default Move Constructor
     * \param other the other concurrent queue to move
     */
    ConcurrentQueue(const ConcurrentQueue &&other);

    /*!
     * \brief operator= Assignment operator
     * \param other the other concurrent queue to copy from.
     * \return itself
     */
    ConcurrentQueue &operator=(const ConcurrentQueue &other);

    /*!
     * \brief operator= Move assignment operator
     * \param other the other concurrent queue to move from.
     * \return itself
     */
    ConcurrentQueue &operator=(const ConcurrentQueue &&other);

    /*!
     * Destructor. Also calls setPopMode(PopModeNonBlocking)
     *
     * \note This destructor is thread-safe.
     */
    ~ConcurrentQueue();

    /*!
     * \brief push Producing function. Adds data onto the queue.
     * \param value The value to store into the queue.
     *
     * \note This function is thread-safe.
     */
    void push(const T &value);

    /*!
     * \brief pop Consuming function. Pops the oldest data entry from the queue. If the queue is empty, the
     * queue will block the consumer thread until the producer adds new data. The user can also optionally unblock the
     * consumer thread by calling setPopMode(PopModeNonBlocking)
     *
     * \param returnValue the value that was popped from the queue
     *
     * \return returns true when returnValue has been populated, false otherwise. If setPopMode(PopModeNonBlocking) was
     * called while the consumer was blocked, it will return false sicne returnValue couldn't be populated.
     *
     * \note This function is thread-safe.
     */
    bool pop(T &returnValue);

    /*!
     * \brief Changes the popMode. If the event that the previous PopMode was set to PopModeBlocking, and we want to
     * change it to PopModeNonBlocking, which will unblock any consumer thread that are blocked on the pop function.
     *
     * \param ePopMode set to PopModeBlocking if you want the threads to block on pop until data becomes available,
     * PopModeNonBlocking otherwise.
     *
     * \note This function is thread-safe.
     */
    void setPopMode(PopMode ePopMode);

    /*!
     * \brief size returns the size of the queue
     * \return a positive number if the queue is not empty, or 0 if the queue is empty.
     *
     * \note This function is thread-safe.
     */
    int32_t getSize() const;

    /*!
     * \brief isBlocking function to see wether the PopMode is blocking or not.
     * \return true if PopMode is set to PopModeBlocking, false otherwise.
     *
     * \note This function is thread-safe.
     */
    bool isBlocking() const;

    /*!
     * \brief clear Clears the queue. Every elements recorded are going to be discarded.
     */
    void clear();

private:

    Mutex                       m_mutex;
    std::condition_variable_any m_unblockedCondition;

    PopMode m_ePopMode;

    Queue m_container;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T, class Queue, class Mutex>
inline ConcurrentQueue<T, Queue, Mutex>::ConcurrentQueue(PopMode ePopMode) :
    m_ePopMode(ePopMode)
{
}

template<class T, class Queue, class Mutex>
inline ConcurrentQueue<T, Queue, Mutex>::ConcurrentQueue(const ConcurrentQueue &other)
{
    std::lock_guard<Mutex> lock(other.m_mutex);
    m_ePopMode = other.m_ePopMode;
    m_container = other.m_container;
}

template<class T, class Queue, class Mutex>
inline ConcurrentQueue<T, Queue, Mutex>::ConcurrentQueue(const ConcurrentQueue &&other) :
    m_ePopMode(m_ePopMode),
    m_container(std::move(other.m_container))
{
}

template<class T, class Queue, class Mutex>
inline ConcurrentQueue<T, Queue, Mutex>::~ConcurrentQueue()
{
    // to unblock any threads just in case. But if the threads are attempting
    setPopMode(PopModeNonBlocking);
}

template<class T, class Queue, class Mutex>
inline ConcurrentQueue<T, Queue, Mutex> &ConcurrentQueue<T, Queue, Mutex>::operator=(const ConcurrentQueue &other)
{
    if (this != &other)
    {
        std::lock_guard<Mutex> otherLock(other.m_mutex);
        std::lock_guard<Mutex> lock(m_mutex);
        m_ePopMode = other.m_ePopMode;
        m_container = other.m_container;
    }
    return *this;
}

template<class T, class Queue, class Mutex>
inline ConcurrentQueue<T, Queue, Mutex> &ConcurrentQueue<T, Queue, Mutex>::operator=(const ConcurrentQueue &&other)
{
    if (this != &other)
    {
        std::lock_guard<Mutex> lock(m_mutex);
        m_ePopMode = other.m_ePopMode;
        m_container = std::move(other.m_container);
    }
    return *this;
}


template<class T, class Queue, class Mutex>
inline void ConcurrentQueue<T, Queue, Mutex>::push(const T &value)
{
    std::lock_guard<Mutex> lock(m_mutex);
    m_container.push(value);
    m_unblockedCondition.notify_one();
}

template<class T, class Queue, class Mutex>

inline bool ConcurrentQueue<T, Queue, Mutex>::pop(T &returnValue)
{
    std::unique_lock<std::mutex> lock(m_mutex);

    // loop to protect against spurious wakeups.
    while (m_container.size() == 0 && m_ePopMode == PopModeBlocking)
    {
        // block until we are unblocked
        m_unblockedCondition.wait(lock);
    }

    if (m_container.size() > 0)
    {
        returnValue = m_container.front();
        m_container.pop();
        return true;
    }

    return false;
}

template<class T, class Queue, class Mutex>
inline void ConcurrentQueue<T, Queue, Mutex>::setPopMode(PopMode ePopMode)
{
    std::lock_guard<Mutex> lock(m_mutex);

    if (m_ePopMode != ePopMode)
    {
        // raise the stop flag
        m_ePopMode = ePopMode;

        if (m_ePopMode == PopModeNonBlocking)
        {
            // notify every blocked thread that
            // the queue is no longer blocking
            m_unblockedCondition.notify_all();
        }
    }
}

template<class T, class Queue, class Mutex>
inline int32_t ConcurrentQueue<T, Queue, Mutex>::getSize() const
{
    std::lock_guard<Mutex> lock(m_mutex);
    return m_container.size();
}

template<class T, class Queue, class Mutex>
inline bool ConcurrentQueue<T, Queue, Mutex>::isBlocking() const
{
    std::lock_guard<Mutex> lock(m_mutex);
    return m_ePopMode == PopModeBlocking;
}

template<class T, class Queue, class Mutex>
inline void ConcurrentQueue<T, Queue, Mutex>::clear()
{
    std::lock_guard<Mutex> lock(m_mutex);
    m_container.clear();
}

#endif // CONCURRENTQUEUE

