/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

#ifndef SPINLOCK_H
#define SPINLOCK_H

#include <atomic>
#include <thread>

/*!
 * \brief The SpinLock class
 *
 * A non-blocking type of mutex that is not putting any thread to sleep, but keep them in a busy loop with regular
 * context switch request to the operating system.
 *
 * The spinlock, like the std::mutex is non-copyable and non-movable
 *
 * \warning This type of mutex should only be used in low-collision and also where locking are not happening for a long
 * period of time. Also, this type of mutex is not recommended for single-core, as it will end up being slower than a
 * regular std::mutex.
 *
 * \todo Check if there is a cross-platform programatic way to detect the amount of core, and use a regular std::mutex
 * when we are running in a single-core environmnent.
 */
class SpinLock
{
public:

    /*!
     * \brief SpinLock Default Constructor
     */
    SpinLock();

    /*!
     * Copy Constructor is disabled.
     */
    SpinLock(const SpinLock &other) = delete;

    /*!
     * Move Constructor is disabled.
     */
    SpinLock(const SpinLock &&other) = delete;

    /*!
     * Assignment operator is disabled.
     */
    SpinLock &operator=(const SpinLock &other) = delete;

    /*!
     * Move assignment operator is disabled.
     */
    SpinLock &operator=(const SpinLock &&other) = delete;

    /*!
     * \brief lock Locks the mutex
     * If the mutex is already locked, it will be stuck in a busy-loop until the owning thread calls unlock().
     */
    void lock();

    /*!
     * \brief try_lock attempts to Lock the mutex
     * If the mutex is already locked, it won't enter in a busy-loop
     *
     * \return returns true if the lock acquisition was successful, false otherwise.
     */
    bool try_lock();

    /*!
     * \brief unlock unlocks the aquired mutex.
     * It will untomatically unblock only a single thread stuck in the lock() function. If the lock was already
     * unlocked, there won't be any effect.
     *
     */
    void unlock();

private:

    std::atomic_flag m_lock;

#if !defined(NDEBUG) // Optimize this out when compiled in release
    std::thread::id  m_owner;
#endif
};

#endif // SPINLOCK_H

