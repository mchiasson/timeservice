/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

#ifndef EVENTQUEUE_H
#define EVENTQUEUE_H

#include <list>
#include <memory>
#include <thread>

// forward declaration
class EventQueuePriv;
class IEvent;

/*!
 * \brief The EventQueue class
 *
 * The event queue class is a class that attaches to its current context. You can only have EventQueue instances per
 * thread because the EventQueue takes ownership of the thread when calling run(), which is a blocking function.
 *
 * The event queue is the perfect mechanism for threads to communicate with each other because they perform the context
 * switch for you, making the handling of the event as simple as it can be: no mutex or any other type of thread safety
 * mechanism necessary.
 */
class EventQueue
{
public:
    /*!
     * \brief EventQueue Constructor
     */
    EventQueue();

    /*!
     * \brief ~EventQueue Destructor
     */
    virtual ~EventQueue();

    /*!
     * \brief postEvent Post an event onto the queue
     *
     * \param pEvent the event instance
     *
     * \note this function is thread safe.
     */
    void postEvent(std::shared_ptr<IEvent> pEvent);

    /*!
     * \brief run
     *
     * This function starts the processing of the events. If no events are found in the queue, it will put the thread to
     * sleep until another thread post an event using psotEvent().
     *
     * The user can chose to subclass EventQueue and override run() if they desire. All the user needs to do is to make
     * sure is to call EventQueue::run() once the user is done doing their custom setup for their own EventQueue
     * implementation.
     *
     * \warning This function is blocking and will take over the current running thread.
     */
    virtual void run();

private:

    std::shared_ptr<EventQueuePriv> m_pPriv;
};

#endif // EVENTQUEUE_H
