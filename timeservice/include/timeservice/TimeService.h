/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

#ifndef TIMESERVICE_H
#define TIMESERVICE_H

#include <list>
#include <memory>
#include <thread>

#include <timeservice/SpinLock.h>
#include <timeservice/ConcurrentQueue.h>

class TimerPriv;
class TimerData;
class TimeServicePriv;

/*!
 * \brief The TimeService class
 *
 * The time service class is essentially a service thread that runs to check a high accuracy clock
 * with a tolerance of less than ~10ms. This tolerance may vary per platform and CPU architecture type.
 *
 * By default, a global instance is available for every Timers to use, which is also accessble through GetInstance().
 * Also, it is possible to instantiate as may TimeService as you desire. A single one should be enough, unless the time
 * drifts too much due to an huge amount of registered Timers. The more timers that are registered to it, the more the
 * timeout will drift, especially if most of those timers are timing out at the same time. We use some logic keep our
 * time expiry sorted to be able to abort when it is no longer necessary to traverse all of the registered timers.
 *
 * The normal case usage is to simply instantiate a Timer without any parameters:
 * \code
 * Timer mytimer;
 * \endcode
 * and the newly instantiated timer will be registered to the global instance automaticlly.
 *
 * The second option is to instantiate your own TimeServices:
 * \code
 * Timer mytimer(TimeService::New());
 * \endcode
 * or
 * \code
 * auto pTimeService = TimeService::New();
 * Timer mytimer1(pTimeService);
 * Timer myTimer2;
 * myTimer2.setTimeService(pTimeService);
 * \endcode
 *
 * As soon as every registered Timers are destroyed, the last Timer isntance will be shutting down and destroying the
 * TimeService for you. You do not need to hold to its shared pointer, unless you want to.
 */
class TimeService
{
    friend class TimerPriv;

public:

    /*!
     * Destructor
     */
    ~TimeService();

    /*!
     * \brief New
     *
     * Helper function to allocate a TimeService. It can be used this way:
     * \code
     * auto pTimeService = TimeService::New();
     * \endcode
     *
     * \return a new instance of TimeService
     *
     * \warning the TimeService thread starts immediately after calling this function.
     */
    static std::shared_ptr<TimeService> New();

    /*!
     * \brief GetInstance
     *
     * This function returns the global TimeService instance. It is the instance used default by every Timer instances
     * that used a default constructor.
     *
     * This instance is also lazy-instantiated, and will self-destruct when the last Timer goes out of scope. It will be
     * reallocated automatically by any newly instantiated Timer instance after that point.
     *
     * \return the global TimeService instance.
     *
     * \warning the global TimeService thread starts immediately after the first use of this function.
     */
    static std::shared_ptr<TimeService> GetInstance();

private:

    TimeService();

    std::shared_ptr<TimeServicePriv> m_pPriv;
};

#endif // TIMESERVICE_H
