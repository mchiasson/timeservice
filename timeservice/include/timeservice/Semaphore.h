/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

#ifndef SEMAPHORE_H
#define SEMAPHORE_H

#include <cassert>
#include <condition_variable>
#include <mutex>

#include <timeservice/SpinLock.h>

/*!
 * Semaphore class
 *
 * It is hard to believe that C++11 still doesn't have a semaphore class. Fortunately, it is not too hard to implement.
 * This class allows the user to specify their own mutex implementation, while providing std::mutex as the default one
 * to use.
 *
 * The semaphore class is non-copyable and non-movable
 *
 * Also, a FastSemaphore variant is available at your convenience, which uses our SpinLock instead of a regular
 * std::mutex.
 *
 *
 */
template<class Mutex = std::mutex>
class Semaphore
{
public:
    /*!
     * \brief Semaphore Constructor
     * \param nStartingResourceCount To specify how many resources should be available. If Unspecified, the resource
     * starting count will start at 0, and the user will need to call release to get the desired available resource
     * count.
     */
    Semaphore(uint32_t nStartingResourceCount = 0);

    /*!
     * Copy constructor is disabled.
     */
    Semaphore(const Semaphore &other) = delete;

    /*!
     * Move constructor is disabled.
     */
    Semaphore(const Semaphore &&other) = delete;

    /*!
     * Destructor
     */
    ~Semaphore();

    /*!
     * Assignment operator is disabled.
     */
    Semaphore &operator=(const Semaphore &other) = delete;

    /*!
     * Move assignment operator is disabled.
     */
    Semaphore &operator=(const Semaphore &&other) = delete;

    /*!
     * \brief tryAcquire Attempt to acquire resources
     *
     * This function will not block if there are not enough resources available in the semaphore
     *
     * \param nResourceCount the amount of resource that we want to acquire.
     *
     * \return true if it was successful, false if there were not enough resources available.
     *
     * \sa acquire
     * \sa release
     */
    bool tryAcquire(uint32_t nResourceCount);

    /*!
     * \brief acquire acquire resources
     *
     * This function will block if there are not enough resrouces available in the semaphore.
     *
     * \param nResourceCount the amount of resource that we want to acquire
     *
     * \sa tryAcquire
     * \sa release
     */
    void acquire(uint32_t nResourceCount);

    /*!
     * \brief release release resrouces
     *
     * When releasing resources, it will notify any thread blocked in the acquire function, and they will reblock if the
     * amount of resource that the are requesting still exceeds the amount of resources available.
     *
     * \param nResourceCount the amount of resource that we want to release (make available for acquiring)
     *
     * \sa acquire
     * \sa tryAcquire
     */
    void release(uint32_t nResourceCount);

    /*!
     * \brief getAvailableResourceCount
     *
     * returns the amount of resources that are currently available.
     *
     * \return available resources
     *
     * \warning This function should not be used to determine if we can acquire or not, because by the time that you
     * query the amount of resource available, the count may have changed by another thread by the time your thread is
     * calling acquire. Please use tryAcquire instead.
     */
    uint32_t getAvailableResourceCount() const;

    /*!
     * \brief getStartingResourceCount returns the starting resource count
     *
     * \note this value cannot be changed.
     */
    uint32_t getStartingResourceCount() const;

private:

    Mutex                       m_mutex;
    std::condition_variable_any m_condition;
    uint32_t                    m_nStartingResourceCount;
    uint32_t                    m_nAvailableResourceCount;
};

typedef Semaphore<SpinLock> FastSemaphore;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template<class Mutex>
inline Semaphore<Mutex>::Semaphore(uint32_t nStartingResourceCount) :
    m_nStartingResourceCount(nStartingResourceCount),
    m_nAvailableResourceCount(nStartingResourceCount)
{
}

template<class Mutex>
inline Semaphore<Mutex>::~Semaphore()
{
    assert(m_nStartingResourceCount == m_nAvailableResourceCount && "One or more semaphore resources were not released.");
}

template<class Mutex>
inline bool Semaphore<Mutex>::tryAcquire(uint32_t nResourceCount)
{
    std::lock_guard<Mutex> lock(m_mutex);
    while (nResourceCount > m_nAvailableResourceCount)
    {
        return false;
    }
    m_nAvailableResourceCount -= nResourceCount;
    return true;
}

template<class Mutex>
inline void Semaphore<Mutex>::acquire(uint32_t nResourceCount)
{
    std::unique_lock<Mutex> lock(m_mutex);
    while (nResourceCount > m_nAvailableResourceCount)
    {
        m_condition.wait(lock);
    }
    m_nAvailableResourceCount -= nResourceCount;
}

template<class Mutex>
inline void Semaphore<Mutex>::release(uint32_t nResourceCount)
{
    std::lock_guard<Mutex> lock(m_mutex);
    assert(nResourceCount < std::numeric_limits<uint32_t>::max() - m_nAvailableResourceCount && "Unsigned integer overflow detected!");
    m_nAvailableResourceCount += nResourceCount;
    m_condition.notify_all();
}

template<class Mutex>
inline uint32_t Semaphore<Mutex>::getAvailableResourceCount() const
{
    std::lock_guard<Mutex> lock(m_mutex);
    return m_nAvailableResourceCount;
}

template<class Mutex>
inline uint32_t Semaphore<Mutex>::getStartingResourceCount() const
{
    return m_nStartingResourceCount;
}

#endif // SEMAPHORE_H

