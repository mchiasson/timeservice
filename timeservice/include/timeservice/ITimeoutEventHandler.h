/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

#ifndef ITIMEOUTEVENTHANDLER_H
#define ITIMEOUTEVENTHANDLER_H

/*!
 * \brief The ITimeoutEventHandler interface
 *
 * Simple interface used by the TimeoutEvent to dispatch the owner of the TimeoutEvent. All the user needs to do is
 * implement the onTimeout() callback to be able to receive TimeoutEvent.
 */
class ITimeoutEventHandler
{
public:
    /*!
     * Default Destructor
     */
    virtual ~ITimeoutEventHandler() = default;

    /*!
     * \brief onTimeout
     *
     * User needs to this function in order to be notified of a TimeoutEvent.
     */
    virtual void onTimeout() = 0;
};


#endif // ITIMEOUTEVENTHANDLER_H

