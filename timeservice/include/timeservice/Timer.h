/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

#ifndef TIMER_H
#define TIMER_H

#include <chrono>
#include <functional>
#include <memory>

#include <timeservice/ITimeoutEventHandler.h>

// Forward Declaration
class TimeService;
class TimerPriv;

/*!
 * \brief The Timer class
 *
 * A nice little timer class that provides an easy API to adjust the timer expiry time, to set wether we want the timer
 * to only execute once or if we want it to be recurring, etc.
 *
 * To be notified from a timeout, there are 2 ways to use this class:
 *
 * 1. You can subclass the Timer class and re-implement the onTimeout() callback
 * 2. Register your own function callback using setTimeoutCallback(), which aslo supports c++11 lambda expressions.
 *
 * \warning this class is not thread safe and should only be accessed by one thread at a time. Using different thread
 * may cause undesireable behaviour because different EventQueue will be used and different threads are going to end up
 * calling your onTimeout() callback or your function callback.
 */
class Timer : public ITimeoutEventHandler
{
public:

    typedef std::function< void(const Timer &) > Callback;

    /*!
     * \brief Timer Constructor
     */
    Timer();

    /*!
     * \brief Timer Constructor that instruct this timer to be using a specific TimeService instance
     * \param pService the service you want this Timer to be notified from
     */
    Timer(std::shared_ptr<TimeService> pService);

    /*!
     * \brief Timer Copy Constructor
     * \param other the other Timer to copy from.
     */
    Timer(const Timer &other);

    /*!
     * \brief Timer Move Constructor
     * \param other the other Timer to move from.
     */
    Timer(const Timer &&other);

    /*!
     * \brief ~Timer Destructor.
     *
     * If the timer is running, it will silently stop it, and unregister automatically from its designated TimerService.
     * If a TimeoutEvent was issued during that time, it will be silently ignored and will not crash your app.
     */
    virtual ~Timer();

    /*!
     * \brief operator= The assignemnt operator
     * \param other the other Timer to copy from.
     * \return itself
     * \warning If the timer was already running, it will be stopped. It is your responsibility to start the timer
     * again.
     */
    Timer &operator=(const Timer &other);

    /*!
     * \brief operator= The move assignemnt operator
     * \param other the other Timer to move from.
     * \return itself
     * \warning If the timer was already running, it will be stopped. It is your responsibility to start the timer
     * again.
     */
    Timer &operator=(const Timer &&other);

    /*!
     * \brief start Starts the timer with a desired interval
     * \param intervalMs the desired interval in milliseconds
     *
     * \note this function has the same effect than calling setInterval() followed by start()
     * \note if the timer is already running, this won't have any effect.
     */
    void start(const std::chrono::milliseconds &intervalMs);

    /*!
     * \brief start Starts the timer
     * \note if the timer is already running, this won't have any effect.
     */
    void start();

    /*!
     * \brief stops the timer
     * \note if the timer is stopped, this won't have any effect.
     */
    void stop();

    /*!
     * \brief isRunning checks if the timer is running
     * \return true if the timer is running, false otherwise
     */
    bool isRunning() const;

    /*!
     * \brief isStopped checks if the timer is not running
     * \return true if the timer is not running, false otherwise
     */
    bool isStopped() const;

    /*!
     * \brief isSingleShot checks if the timer is singleShot
     *
     * If the timer was set to singleShot, the timeout event will only happens once and the timer will stop
     * automatically after that first timeout. The default value is false. To turn this singleshot mode on, you only
     * need to call setSingleShot(true).
     *
     * \return true if the timer is a singleshot timer, false otherwise.
     */
    bool isSingleShot() const;

    /*!
     * \brief setSingleShot
     *
     * This function turns the single shot mode on or off.
     *
     * \param bIsSingleShot set to true if you want the timer to be singleShot, false otherwise.
     */
    void setSingleShot(bool bIsSingleShot);

    /*!
     * \brief getInterval
     *
     * \return returns the timer interval.
     */
    const std::chrono::milliseconds &getInterval() const;

    /*!
     * \brief setInterval
     *
     * Let you adjust the timer interval, in milliseconds.
     *
     * \param intervalMs
     */
    void setInterval(const std::chrono::milliseconds &intervalMs);

    /*!
     * \brief getTimeoutCallback
     *
     * returns the last callback that was set. Only one callback can be set at a time.
     *
     * \return if the callback was never set before, it will return null.
     */
    const Callback &getTimeoutCallback() const;

    /*!
     * \brief setTimeoutCallback
     *
     * Lets you set the timeout callback. You can use a regular function, or a method, or even a lamda expression
     *
     * Example:
     * \code
     * myTimer.setTimeoutCallback([&](const Timer &otherTimer) { printf("timeout!\n"); });
     * \endcode
     *
     * \param timeoutCallback
     */
    void setTimeoutCallback(const Callback &timeoutCallback);

    /*!
     * \brief getTimeService
     * \return returns the assigned TimeService that the timer currently uses.
     */
    std::shared_ptr<TimeService> getTimeService() const;

    /*!
     * \brief setTimeService
     * \param pService the TimeService that this timer should be using.
     *
     * \warning If the timer was already running, it will be stopped. It is your responsibility to start the timer
     * again.
     */
    void setTimeService(std::shared_ptr<TimeService> pService);

    /*!
     * \brief onTimeout default callback.
     *
     * Currently does nothing. The user can chose to subclass the Timer class and re-implement this hook to be notified
     * from timeout events, or they can chose to use the setTimeoutCallback() method to register any callback that you
     * desire.
     */
    virtual void onTimeout() override;
private:

    std::shared_ptr<TimerPriv> m_pPriv;

};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline void Timer::onTimeout()
{
    // no-op.
}

#endif // TIMER_H

