/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

#ifndef TIMEOUTEVENT_H
#define TIMEOUTEVENT_H

#include <timeservice/IEvent.h>

#include <memory>

// forward declaration
class ITimeoutEventHandler;

/*!
 * \brief The TimeoutEvent class
 *
 * This is a specialized event designed to notify a corresponding ITimeoutEventHandler that a timeout occured. This
 * class is principally used internally by the TimeService class when a timeout occured.
 *
 * It holds a weak pointer to the ITimeoutEventHandler, so if the handler is destroyed, it will be able to prevent a
 * crash from its detectable nullptr.
 */
class TimeoutEvent : public IEvent
{
public:
    /*!
     * \brief TimeoutEvent Constructor
     * \param pEventHandler the event handler to call back
     */
    TimeoutEvent(std::shared_ptr<ITimeoutEventHandler> pEventHandler);

    /*!
     * \brief execute the execution of this event.
     *
     * This function calls ITimeoutEventHandler::onTimeout() if the handler hadn't been deleted while the event was
     * queued.
     */
    virtual void execute() override;

private:

    std::weak_ptr<ITimeoutEventHandler> m_pEventHandler;
};

#endif // TIMEOUTEVENT_H
