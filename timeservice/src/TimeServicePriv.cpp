/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

#include "TimeServicePriv.h"

#include <algorithm>
#include <mutex>

#include "timeservice/TimeoutEvent.h"
#include "EventQueuePriv.h"
#include "TimerData.h"
#include "TimerPriv.h"

std::shared_ptr<TimeService> TimeServicePriv::GetInstance()
{
    auto pGlobalTimeService = s_pGlobalTimeService.lock();

    if (!pGlobalTimeService)
    {
        std::lock_guard<SpinLock> lock(s_globalTimeServiceMutex);
        if (!pGlobalTimeService)
        {
            pGlobalTimeService = std::shared_ptr<TimeService>(TimeService::New());
            s_pGlobalTimeService = pGlobalTimeService;
        }
    }

    return pGlobalTimeService;
}

void TimeServicePriv::registerTimer(std::shared_ptr<TimerPriv> pTimer)
{
    auto pData = std::unique_ptr<TimerData>(new TimerData { std::weak_ptr<TimerPriv>(pTimer), pTimer->getInterval(),
                                                            pTimer->isSingleShot(),
                                                            std::this_thread::get_id(),
                                                            std::chrono::high_resolution_clock::now() + pTimer->getInterval()} );

    std::lock_guard<SpinLock> lock(m_timerListMutex);

    auto it  = m_timerList.begin();
    auto end = m_timerList.end();

    // since the list is sorted, we can use a binary search to find the duplicate value faster
    if (std::binary_search(it, end, pData) == false)
    {
        // binary insert
        m_timerList.emplace(std::lower_bound(m_timerList.begin(), m_timerList.end(), pData), std::move(pData));
    }
}

void TimeServicePriv::unregisterTimer(std::shared_ptr<TimerPriv> pTimer)
{
    std::lock_guard<SpinLock> writeLock(m_timerListMutex);

    auto it = m_timerList.begin();
    auto end = m_timerList.end();
    while (it != end)
    {
        if (pTimer == (*it)->m_pTimer.lock())
        {
            m_timerList.erase(it);
            break;
        }
        ++it;
    }
}

TimeServicePriv::TimeServicePriv() :
    m_bStopRequested(false)
{
    m_thread = std::thread([&]()
    {
        while (m_bStopRequested == false)
        {
            // Capture the time before the loop. Querying the time is expensive, so this way, it will cost less but the
            // time can drift while traversing the list.
            auto now = std::chrono::high_resolution_clock::now();

            std::lock_guard<SpinLock> lock(m_timerListMutex);

            // loop until we processed every expired timers. the expired timers always appear at the top since the list is
            // sorted. We also abort when the list is empty.
            while (m_timerList.size() > 0 && now >= m_timerList.front()->m_endTime)
            {
                // pop the first
                std::unique_ptr<TimerData> pData = std::move(m_timerList.front());
                m_timerList.pop_front();

                // make sure the timer pointer hasn't been deleted
                std::shared_ptr<TimerPriv> pTimer = pData->m_pTimer.lock();
                if (pTimer)
                {
                    // post our timeout event
                    for (auto &pEventQueue : EventQueuePriv::GetEventQueues(pData->m_threadAffinity))
                    {
                        pEventQueue->postEvent(std::make_shared<TimeoutEvent>(pTimer));
                    }

                    // Re-post it in our timer queue if an only if it is not a single shot timer
                    if (pData->m_bIsSingleShot == false)
                    {
                        // recalculate the new expiry time
                        pData->m_endTime = now + pData->m_intervalMs;

                        // binary re-insert
                        m_timerList.emplace(std::lower_bound(m_timerList.begin(), m_timerList.end(), pData), std::move(pData));
                    }

                }

            }

            // now sleep for a very small amount of time to avoid burning our CPU.
            std::this_thread::sleep_for(std::chrono::microseconds(1));
        }
    });
}

TimeServicePriv::~TimeServicePriv()
{
    m_bStopRequested = true;
    m_thread.join();
}

std::weak_ptr<TimeService> TimeServicePriv::s_pGlobalTimeService;
SpinLock                   TimeServicePriv::s_globalTimeServiceMutex;
