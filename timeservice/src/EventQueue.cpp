/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

#include "timeservice/EventQueue.h"

#include "timeservice/IEvent.h"
#include "EventQueuePriv.h"

#include <algorithm>

EventQueue::EventQueue() :
    m_pPriv(new EventQueuePriv)
{
    m_pPriv->m_pPub = this;
    std::lock_guard<SpinLock> lock(EventQueuePriv::s_eventQueueMapMutex);
    EventQueuePriv::s_eventQueueMap.insert(make_pair(std::this_thread::get_id(), m_pPriv));
}

EventQueue::~EventQueue()
{
    m_pPriv->m_pPub = nullptr;
}

void EventQueue::postEvent(std::shared_ptr<IEvent> pEvent)
{
    m_pPriv->postEvent(pEvent);
}

void EventQueue::run()
{
    m_pPriv->run();
}
