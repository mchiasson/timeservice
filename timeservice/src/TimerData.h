/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

#ifndef TIMERDATA_H
#define TIMERDATA_H

#include <chrono>
#include <thread>

// forward declaration
class TimerPriv;

/*!
 * This is a private struct. It was made so that the TimeService (which runs its own thread) can make a copy of the
 * timer data for thread safety + performance consideration.
 */
struct TimerData
{
    std::weak_ptr<TimerPriv>  m_pTimer;
    std::chrono::milliseconds m_intervalMs;
    bool                      m_bIsSingleShot;
    std::thread::id           m_threadAffinity;

    // warning : this value is constantly updated by the time service thread.
    std::chrono::high_resolution_clock::time_point m_endTime;
};

// for std::lower_bound binary-insert trick
inline bool operator<(const std::unique_ptr<TimerData> &pLHS, const std::unique_ptr<TimerData> &pRHS)
{
    return pLHS->m_endTime < pRHS->m_endTime;
}

#endif // TIMERDATA_H
