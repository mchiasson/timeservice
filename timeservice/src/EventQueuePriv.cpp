/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

#include "EventQueuePriv.h"

#include "timeservice/IEvent.h"

#include <algorithm>

EventQueuePriv::EventQueuePriv() :
    m_pPub(nullptr),
    m_queue(PopModeBlocking)
{
}

EventQueuePriv::~EventQueuePriv()
{
    std::lock_guard<SpinLock> lock(s_eventQueueMapMutex);

    s_eventQueueMap.erase(std::find_if(s_eventQueueMap.begin(),
                                       s_eventQueueMap.end(),
                                       [=](const std::pair<std::thread::id, std::shared_ptr<EventQueuePriv> > &pair) { return pair.second == shared_from_this();}));
}

void EventQueuePriv::postEvent(std::shared_ptr<IEvent> pEvent)
{
    m_queue.push( pEvent );
}

void EventQueuePriv::run()
{
    std::shared_ptr<IEvent> pEvent;
    while (m_queue.pop(pEvent))
    {
        if (pEvent)
        {
            pEvent->execute();
            pEvent.reset();
        }
    }
}

std::vector< std::shared_ptr<EventQueuePriv> > EventQueuePriv::GetEventQueues(std::thread::id threadId)
{
    std::vector< std::shared_ptr<EventQueuePriv> > results;

    std::lock_guard<SpinLock> lock(s_eventQueueMapMutex);

    auto keyRange = s_eventQueueMap.equal_range(threadId);
    while (keyRange.first != keyRange.second)
    {
        results.push_back((*keyRange.first).second);
        ++keyRange.first;
    }

    return results; // automatic std::move apparently. Thank you C++11!
}

std::multimap<std::thread::id, std::shared_ptr<EventQueuePriv> > EventQueuePriv::s_eventQueueMap;
SpinLock                                                         EventQueuePriv::s_eventQueueMapMutex;
