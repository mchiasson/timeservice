/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

#include "timeservice/Timer.h"

#include "timeservice/TimeService.h"
#include "TimerData.h"
#include "TimerPriv.h"

#include <iostream>

Timer::Timer() :
    m_pPriv(new TimerPriv())
{
    m_pPriv->m_pPub = this;
}

Timer::Timer(std::shared_ptr<TimeService> pService) :
    m_pPriv(new TimerPriv(pService))
{
    m_pPriv->m_pPub = this;
}

Timer::Timer(const Timer &other) :
    m_pPriv(new TimerPriv(*other.m_pPriv))
{
    m_pPriv->m_pPub = this;
}

Timer::Timer(const Timer &&other) :
    m_pPriv(new TimerPriv(std::move(*other.m_pPriv)))
{
    m_pPriv->m_pPub = this;
}

Timer::~Timer()
{
    m_pPriv->m_pPub = nullptr;
}

Timer &Timer::operator=(const Timer &other)
{
    if (this != &other)
    {
        *m_pPriv = *other.m_pPriv;
    }

    return *this;
}

Timer &Timer::operator=(const Timer &&other)
{
    if (this != &other)
    {
        m_pPriv = std::move(other.m_pPriv);
    }

    return *this;
}

void Timer::start(const std::chrono::milliseconds &intervalMs)
{
    m_pPriv->start(intervalMs);
}

void Timer::start()
{
    m_pPriv->start();
}

void Timer::stop()
{
    m_pPriv->stop();
}

void Timer::setTimeService(std::shared_ptr<TimeService> pService)
{
    m_pPriv->setTimeService(pService);
}

bool Timer::isRunning() const
{
    return m_pPriv->isRunning();
}

bool Timer::isStopped() const
{
    return m_pPriv->isStopped();
}

bool Timer::isSingleShot() const
{
    return m_pPriv->isSingleShot();
}

void Timer::setSingleShot(bool bIsSingleShot)
{
    m_pPriv->setSingleShot(bIsSingleShot);
}

const std::chrono::milliseconds &Timer::getInterval() const
{
    return m_pPriv->getInterval();
}

void Timer::setInterval(const std::chrono::milliseconds &intervalMs)
{
    m_pPriv->setInterval(intervalMs);
}

const Timer::Callback &Timer::getTimeoutCallback() const
{
    return m_pPriv->getTimeoutCallback();
}

void Timer::setTimeoutCallback(const Callback &timeoutCallback)
{
    m_pPriv->setTimeoutCallback(timeoutCallback);
}

std::shared_ptr<TimeService> Timer::getTimeService() const
{
    return m_pPriv->getTimeService();
}
