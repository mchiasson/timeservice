/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

#include "TimerPriv.h"

#include "timeservice/TimeService.h"
#include "TimerData.h"
#include "TimeServicePriv.h"

#include <iostream>

// This implementaton cannot be inlined because it requires TimeService.h
TimerPriv::TimerPriv() :
    m_state(StateStopped),
    m_intervalMs(0),
    m_bIsSingleShot(false),
    m_pService(TimeService::GetInstance())
{
}

TimerPriv::~TimerPriv()
{
    if (m_state == StateRunning)
    {
        if (m_pService)
        {
            m_pService->m_pPriv->unregisterTimer(shared_from_this());
        }
        m_state = StateStopped;
    }
    m_intervalMs = std::chrono::milliseconds(0);
    m_bIsSingleShot = false;
}

TimerPriv &TimerPriv::operator=(const TimerPriv &other)
{
    if (this != &other)
    {
        if (m_state == StateRunning)
        {
            if (m_pService)
            {
                m_pService->m_pPriv->unregisterTimer(shared_from_this());
                std::cerr << "Warning : TimerPriv '0x" << this << "' stopped because the TimerPriv was overwritten using the assignment operator while running." << std::endl;
            }
            m_state = StateStopped;
        }

        m_intervalMs      = other.m_intervalMs;
        m_bIsSingleShot   = other.m_bIsSingleShot;
        m_timeoutCallback = other.m_timeoutCallback;
        m_pService        = other.m_pService;
    }

    return *this;
}

TimerPriv &TimerPriv::operator=(const TimerPriv &&other)
{
    if (this != &other)
    {
        if (m_state == StateRunning)
        {
            if (m_pService)
            {
                m_pService->m_pPriv->unregisterTimer(shared_from_this());
                std::cerr << "Warning : TimerPriv '0x" << this << "' stopped because the TimerPriv was overwritten using the move assignment operator while running." << std::endl;
            }
            m_state = StateStopped;
        }

        m_intervalMs      = std::move(other.m_intervalMs);
        m_bIsSingleShot   = std::move(other.m_bIsSingleShot);
        m_timeoutCallback = std::move(other.m_timeoutCallback);
        m_pService        = std::move(other.m_pService);
    }

    return *this;
}

void TimerPriv::start(const std::chrono::milliseconds &intervalMs)
{
    m_intervalMs = intervalMs;
    start();
}

void TimerPriv::start()
{
    if (m_state == StateStopped)
    {
        if (m_pService)
        {
            m_pService->m_pPriv->registerTimer(shared_from_this());
            m_state = StateRunning;
        }
        else
        {
            std::cerr << "Could not start TimerPriv '0x" << this << "' because it is not registered to a time service." << std::endl;
        }
    }
    else
    {
        std::cerr << "TimerPriv '0x" << this << "' is already running." << std::endl;
    }
}

void TimerPriv::stop()
{
    if (m_state == StateRunning)
    {
        if (m_pService)
        {
            m_pService->m_pPriv->unregisterTimer(shared_from_this());
            m_state = StateStopped;
        }
        else
        {
            // this should never happen.
            std::cerr << "Could not stop TimerPriv '0x" << this << "' because it is not registered to a time service." << std::endl;
        }
    }
    else
    {
        std::cerr << "TimerPriv '0x" << this << "' is already stopped." << std::endl;
    }
}

void TimerPriv::setTimeService(std::shared_ptr<TimeService> pService)
{
    if (m_state == StateRunning)
    {
        if (m_pService)
        {
            m_pService->m_pPriv->unregisterTimer(shared_from_this());
            std::cerr << "Warning : TimerPriv '0x" << this << "' stopped because the TimerPriv service was changed while running." << std::endl;
        }
        m_state = StateStopped;
    }

    m_pService = pService;
}

void TimerPriv::onTimeout()
{
    if (m_pPub && m_state == StateRunning)
    {
        // set the state to stopped if we want to be able to call start() again.
        if (m_bIsSingleShot)
        {
            m_state = StateStopped;
        }

        // call our timout callbacks
        if (m_timeoutCallback)
        {
            m_timeoutCallback(*m_pPub);
        }
        m_pPub->onTimeout();

    }
}
