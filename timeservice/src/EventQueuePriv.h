/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

#ifndef EVENTQUEUEPRIV_H
#define EVENTQUEUEPRIV_H

#include <timeservice/ConcurrentQueue.h>
#include <timeservice/SpinLock.h>
#include <timeservice/EventQueue.h>

#include <list>
#include <map>
#include <memory>
#include <thread>

class IEvent;

class EventQueuePriv : public std::enable_shared_from_this<EventQueuePriv>
{
    friend class EventQueue;

public:

    EventQueue *m_pPub;

    EventQueuePriv();
    ~EventQueuePriv();

    void postEvent(std::shared_ptr<IEvent> pEvent);

    void run();

    static std::vector< std::shared_ptr<EventQueuePriv> > GetEventQueues(std::thread::id threadId);

private:

    ConcurrentQueue< std::shared_ptr<IEvent> > m_queue;

    static std::multimap<std::thread::id, std::shared_ptr<EventQueuePriv> > s_eventQueueMap;
    static SpinLock                                                         s_eventQueueMapMutex;
};
#endif // EVENTQUEUEPRIV_H
