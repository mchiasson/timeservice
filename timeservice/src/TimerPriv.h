/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

#ifndef TIMERPRIV_H
#define TIMERPRIV_H

#include <chrono>
#include <functional>
#include <memory>

#include <timeservice/ITimeoutEventHandler.h>
#include <timeservice/Timer.h>

// Forward Declaration
class TimeService;

class TimerPriv : public ITimeoutEventHandler, public std::enable_shared_from_this<TimerPriv>
{
public:

    Timer *m_pPub;

    enum State
    {
        StateStopped = 0,
        StateRunning,
    };

    TimerPriv();
    TimerPriv(std::shared_ptr<TimeService> pService);
    TimerPriv(const TimerPriv &other);
    TimerPriv(const TimerPriv &&other);
    virtual ~TimerPriv();

    TimerPriv &operator=(const TimerPriv &other);
    TimerPriv &operator=(const TimerPriv &&other);

    void start(const std::chrono::milliseconds &intervalMs);
    void start();
    void stop();

    bool isRunning() const;
    bool isStopped() const;

    bool isSingleShot() const;
    void setSingleShot(bool bIsSingleShot);

    const std::chrono::milliseconds &getInterval() const;
    void setInterval(const std::chrono::milliseconds &intervalMs);

    const Timer::Callback &getTimeoutCallback() const;
    void setTimeoutCallback(const Timer::Callback &timeoutCallback);

    std::shared_ptr<TimeService> getTimeService() const;
    void setTimeService(std::shared_ptr<TimeService> pService);

    virtual void onTimeout() override;

private:

    State                 m_state;
    std::chrono::milliseconds    m_intervalMs;
    bool                         m_bIsSingleShot;
    Timer::Callback              m_timeoutCallback;
    std::shared_ptr<TimeService> m_pService;

};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline TimerPriv::TimerPriv(std::shared_ptr<TimeService> pService) :
    m_state(StateStopped),
    m_intervalMs(0),
    m_bIsSingleShot(false),
    m_pService(pService)
{
}

inline TimerPriv::TimerPriv(const TimerPriv &other) :
    std::enable_shared_from_this<TimerPriv>(other),
    m_state(StateStopped),
    m_intervalMs(other.m_intervalMs),
    m_bIsSingleShot(other.m_bIsSingleShot),
    m_pService(other.m_pService)
{
}

inline TimerPriv::TimerPriv(const TimerPriv &&other) :
    std::enable_shared_from_this<TimerPriv>(other),
    m_state(StateStopped),
    m_intervalMs(std::move(other.m_intervalMs)),
    m_bIsSingleShot(std::move(other.m_bIsSingleShot)),
    m_pService(std::move(other.m_pService))
{
}

inline bool TimerPriv::isRunning() const
{
    return m_state == StateRunning;
}

inline bool TimerPriv::isStopped() const
{
    return m_state == StateStopped;
}

inline bool TimerPriv::isSingleShot() const
{
    return m_bIsSingleShot;
}

inline void TimerPriv::setSingleShot(bool bIsSingleShot)
{
    m_bIsSingleShot = bIsSingleShot;
}

inline const std::chrono::milliseconds &TimerPriv::getInterval() const
{
    return m_intervalMs;
}

inline void TimerPriv::setInterval(const std::chrono::milliseconds &intervalMs)
{
    m_intervalMs = intervalMs;
}

inline const Timer::Callback &TimerPriv::getTimeoutCallback() const
{
    return m_timeoutCallback;
}

inline void TimerPriv::setTimeoutCallback(const Timer::Callback &timeoutCallback)
{
    m_timeoutCallback = timeoutCallback;
}

inline std::shared_ptr<TimeService> TimerPriv::getTimeService() const
{
    return m_pService;
}

#endif // TIMERPRIV_H
