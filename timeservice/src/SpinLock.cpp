/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

#include "timeservice/SpinLock.h"

#include <cassert>
#include <thread>

SpinLock::SpinLock() :
    m_lock(ATOMIC_FLAG_INIT)
{
}

void SpinLock::lock()
{
    while (true)
    {
        for (int32_t i = 0; i < 10000; ++i)
        {
            if (!m_lock.test_and_set(std::memory_order_acquire))
            {
#if !defined(NDEBUG) // Optimize this out when compiled in release
                // record the owner (useful for debug purpose)
                m_owner = std::this_thread::get_id();
#endif
                return;
            }
        }

        std::this_thread::yield();
    }
}

bool SpinLock::try_lock()
{
    bool bSuccess = !m_lock.test_and_set(std::memory_order_acquire);
#if !defined(NDEBUG) // Optimize this out when compiled in release
    if (bSuccess)
    {
        // record the owner (useful for debug purpose)
        m_owner = std::this_thread::get_id();
    }
#endif
    return bSuccess;
}

void SpinLock::unlock()
{
    assert(m_owner == std::this_thread::get_id() && "Current thread is not the owner of the lock.");
#if !defined(NDEBUG) // Optimize this out when compiled in release
    m_owner = std::thread::id(); // reset the owner.
#endif
    m_lock.clear(std::memory_order_release);
}
