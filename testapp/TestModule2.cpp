/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/
#include "TestModule2.h"

#include <iostream>
#include <functional>

TestModule2::TestModule2()
{
    // then let's assign a member function callbacks to see some things on the screen
    m_timer1.setTimeoutCallback(Timer::Callback(std::bind(&TestModule2::onTimeout, this, std::placeholders::_1)));

    // then let's start both timers:
    m_timer1.start(std::chrono::milliseconds(3050));
}


void TestModule2::onTimeout(const Timer &timer)
{
    if (&m_timer1 == &timer)
    {
        std::cout << "TestModule2::m_timer1 timed out from a member function here!" << std::endl;
    }
    else
    {
        std::cerr << "TestModule2 error : What happened?" << std::endl;
    }

}
