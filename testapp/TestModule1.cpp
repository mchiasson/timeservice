/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

#include "TestModule1.h"

#include <iostream>

TestModule1::TestModule1()
{
    // let's make m_timer1 and m_time3 recurring, and m_timer2 single shot
    m_timer2.setSingleShot(true);

    // and then let's assign some lamda callbacks to see some things on the screen
    m_timer1.setTimeoutCallback([](const Timer &){ std::cout << "TestModule1::m_timer1 timed out!" << std::endl; });
    m_timer2.setTimeoutCallback([](const Timer &){ std::cout << "TestModule1::m_timer2 (singleshot) timed out!" << std::endl; });
    m_timer3.setTimeoutCallback([](const Timer &){ std::cout << "TestModule1::m_timer3 timed out!" << std::endl; });

    // then let's start both timers:
    m_timer1.start(std::chrono::milliseconds(500));
    m_timer2.start(std::chrono::milliseconds(2225));
    m_timer3.start(std::chrono::milliseconds(745));

}


