/*****************************************************************************
The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

#include <timeservice/EventQueue.h>

#include "TestModule1.h"
#include "TestModule2.h"
#include "TestModule3.h"

int main()
{
    std::thread testModule1thread([=](){

        // instantiate our TestModule1
        TestModule1 module;

        // and launch our event queue.
        EventQueue eventQueue;
        eventQueue.run();
    });

    std::thread testModule2thread([=](){

        // instantiate our TestModule2
        TestModule2 module;

        // and launch our event queue.
        EventQueue eventQueue;
        eventQueue.run();
    });

    std::thread testModule3thread([=](){

        // instantiate our TestModule3
        TestModule3 module;

        // and launch our event queue.
        EventQueue eventQueue;
        eventQueue.run();
    });


    testModule1thread.detach();
    testModule2thread.detach();
    testModule3thread.detach();

    // let's just hold this program for 15 seconds, and then let it go.
    std::this_thread::sleep_for(std::chrono::seconds(15));

    return 0;
}
