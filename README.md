The MIT License (MIT)

Copyright © 2015 Mathieu-Andre Chiasson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


# TimeService
A fast time service library written in C++11 which includes a registration mechanism to allow objects to be notified from a timeout event when a timeout occurs.

To clone, please use the following command

Example:

```bash
git clone https://bitbucket.org/mchiasson/timeservice.git
```

or (for SSH users)

```bash
git clone git@bitbucket.org:mchiasson/timeservice.git
```

# Special requirement for Linux user

Linux users must have a C++ development environment installed, git and cmake 

For ubuntu/mint/debian users:

```bash
sudo apt-get install build-essential git cmake
```

# How to build

First, generate your makefiles

```bash
cd TimeService
cmake .
```

And then build

```bash
make
```

# Eclipse IDE

It is also possible to instruct cmake to generate an eclipse environment if you prefer. This is can be done with the following command:

```bash
cmake -G "Eclipse CDT4 - Unix Makefiles" .
```

# How to run

go to the testapp folder, and launch testapp

```bash
cd testapp
./testapp
```

Enjoy!
